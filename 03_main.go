// package main
package main

import (
// 	"github.com/tkstorm/go-design/structural/decorator/decolog"
	"log"
	"math/rand"
	"time"
)


//Decorate the operation

func main() {
	// decorate log a
	Decorate(DoActionA)()
	// decorate log b
	Decorate(DoActionB)()
}

func DoActionA() {
	time.Sleep(time.Duration(rand.Intn(200)) * time.Millisecond)
	log.Println("finish action a")
}

func DoActionB() {
	time.Sleep(time.Duration(rand.Intn(200)) * time.Millisecond)
	log.Println("finish action b")
}

// 2019/10/30 13:28:46 finish action a
// 2019/10/30 13:28:46 elapsed time 84
// 2019/10/30 13:28:46 finish action b
// 2019/10/30 13:28:46 elapsed time 87
