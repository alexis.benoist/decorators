def decorateur(function):
    def new_function(*args, **kwargs):
        print('Begin')
        value = function(*args, **kwargs)
        print('End')
        return value

    return new_function


@decorateur
def hello():
    print('Hello')


print(hello.__name__)
