def decorateur(function):
    def new_function(*args, **kwargs):
        print('Begin')
        function(*args, **kwargs)
        print('End')

    return new_function


@decorateur
def hello():
    print('Hello')


@decorateur
def add(x, y):
    return x + y


hello()
print('')
add(1, 1)
