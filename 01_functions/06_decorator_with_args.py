def decorateur_1(function):
    def new_function(*args, **kwargs):
        print('Begin 1')
        function(*args, **kwargs)
        print('End 1')

    return new_function


def decorateur_2(function):
    def new_function(*args, **kwargs):
        print('Begin 2')
        function(*args, **kwargs)
        print('End 2')

    return new_function


@decorateur_1
@decorateur_2
def hello():
    print('Hello')


hello()
