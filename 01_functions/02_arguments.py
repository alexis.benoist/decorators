def decorateur(function):
    def new_function():
        print('Begin')
        function()
        print('End')
    return new_function


@decorateur
def add(x, y):
    return x + y


add(1, 1)
