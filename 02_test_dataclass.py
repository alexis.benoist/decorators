from dataclasses import _init_fn, _get_field
from dataclasses import dataclass


def init_fn(fields):
    return _init_fn(fields=fields, frozen=True, has_post_init=False, self_name='self')



def test_one_attributes():
    @dataclass
    class A:
        s: str

    a = A(s='s')
    assert a.s == 's'


def test_two_attributes():
    @dataclass
    class A:
        s: str
        d: int

    a = A(s='s', d=1)
    assert a.s == 's'
    assert a.d == 1
